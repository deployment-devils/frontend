describe('visit morning news', () => {
  it('visit', () => {
    cy.visit('http://localhost:3000')
  })
  it('create a new user', () => {
    cy.visit('https://the-deployments-devils-frontend.vercel.app/')
    cy.get('.fa-user').click()
    cy.get('#signUpUsername').type('new-test')
    cy.get('#signUpPassword').type('test')
    cy.get('#register').click()
  })
  it('logs the new user', () => {
    cy.visit('https://the-deployments-devils-frontend.vercel.app/')
    cy.get('.fa-user').click()
    cy.get('#signInUsername').type('test')
    cy.get('#signInPassword').type('test')
    cy.get('#connection').click()
    cy.get('.Header_logoutSection__KHqw2').contains("Welcome user_test")
  })
})
